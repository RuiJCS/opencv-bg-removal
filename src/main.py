import cv2
import sys
import numpy as np

GAUSSIAN_NOISE_KERNEL = 51
GRADIENT_KERNEL_SIZE = 21
X = 0
Y = 0
SAVE = False


def fetch_click_coordinates(event, x, y, flags, param):
    global X
    global Y
    global SAVE
    if event == cv2.EVENT_LBUTTONDOWN:
        X = x
        Y = y
    if event == cv2.EVENT_FLAG_ALTKEY:
        SAVE = True


def mask_creator(input_frame):
    frame = input_frame
    frame = cv2.medianBlur(frame, GAUSSIAN_NOISE_KERNEL)
    frame = cv2.GaussianBlur(frame, (GAUSSIAN_NOISE_KERNEL, GAUSSIAN_NOISE_KERNEL), 0)
    # sobelx = cv2.Scharr(frame, cv2.CV_8U, 1, 0, borderType=cv2.BORDER_REPLICATE)
    # sobely = cv2.Scharr(frame, cv2.CV_8U, 0, 1, borderType=cv2.BORDER_REPLICATE)
    # sobelx = cv2.Sobel(frame, cv2.CV_8U, 1, 0, borderType=cv2.BORDER_REPLICATE)
    # sobely = cv2.Sobel(frame, cv2.CV_8U, 0, 1, borderType=cv2.BORDER_REPLICATE)
    # frame = cv2.addWeighted(sobelx, 0.5, sobely, 0.5, 0)
    # frame = cv2.normalize(frame, frame, alpha=255)

    frame = cv2.Laplacian(frame, cv2.CV_8U)
    # Apply the operations
    frame = cv2.cvtColor(input_frame, cv2.COLOR_BGR2GRAY)
    frame = 255 - frame
    frame = cv2.equalizeHist(frame)
    kernel = np.ones((GRADIENT_KERNEL_SIZE, GRADIENT_KERNEL_SIZE), np.uint8)
    # frame = cv2.morphologyEx(frame, cv2.MORPH_GRADIENT, kernel)
    frame = cv2.morphologyEx(frame, cv2.MORPH_CLOSE, kernel)
    ret, frame = cv2.threshold(frame, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
    # frame = cv2.adaptiveThreshold(
    #     frame, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 3, 0
    # )
    h, w = frame.shape[:2]
    mask = frame.copy()
    mask = cv2.resize(mask, (w + 2, h + 2))
    cv2.floodFill(frame, mask, (X, Y), 255)
    frame = cv2.bitwise_not(frame)
    return frame


def process(img):
    gs = mask_creator(img)
    gs = cv2.bitwise_and(img, img, mask=gs)
    return gs


def show_image(img):
    global SAVE
    if SAVE:
        cv2.imwrite("out.jpg", img)
        SAVE = False
    cv2.namedWindow(winname="foreground")
    cv2.setMouseCallback("foreground", fetch_click_coordinates)
    cv2.imshow("foreground", img)


def main(image_name):
    cv2.namedWindow(winname="foreground")
    cv2.setMouseCallback("foreground", fetch_click_coordinates)
    img = cv2.imread(image_name)
    gs = mask_creator(img.copy())
    gs = cv2.bitwise_and(img, img, mask=gs)
    while True:
        cv2.imshow("foreground", gs)
        key = cv2.waitKey(10) & 0xFF

        if key == 27:
            break


if __name__ == "__main__":
    main(sys.argv[1])
