import cv2
from main import process, show_image

cam = cv2.VideoCapture(0)

ret, frame = cam.read()
if ret is True:
    run = True
else:
    run = False

while run:
    # Read a frame from the camera
    ret, frame = cam.read()

    # If the frame was properly read.
    if ret is True:
        img = process(frame)
        show_image(img)
        key = cv2.waitKey(10) & 0xFF
    else:
        break

    if key == 27:
        break

cam.release()
cv2.destroyAllWindows()
